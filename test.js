const Asmp = require('./index')

;(async () => {

  let asmp = Asmp
  
  // proxy設定する
  asmp.defaults()
  asmp.blockedStatusCodes = [201]

  // 成功する
  await asmp.get('http://www.feawhorarabre.graereabaer/')

  // ブロッキングされたとしてブロックする
  asmp.blockedStatusCodes = [200]
  await asmp.get('http://www.feawhorarabre.graereabaer/')
})()
