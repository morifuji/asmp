const request = require('request-promise')
const childProcess = require('child_process')

let proxyList = []

// 設定値
let blockedStatusCodes = [500]
let amiId = 'ami-0581c4e3006a1a0af'
let port = '60088'
let tagName = 'crawling-child'
let instanceType = 't2.micro'
let sshKeyName = 'crawling-key'
let securityGroups = 'crawling-rule'

class Asmp{
  static async get(url) {
    const proxy = this.getRandomProxy()
    return request.get(url, this.getOption(proxy)).catch(async res => {
      if (this.isBlocked(res)) {
        this.startProxyServer()
        this.killProxyServer(proxy)
        proxyList = Asmp.listProxyServer()
      }
      throw res
    })
  }

  /**
   * setter
   */
  static set blockedStatusCodes(v) {
    blockedStatusCodes = v
  }
  static set amiId(v) {
    amiId = v
  }
  static set port(v) {
    port = v
  }
  static set tagName(v) {
    tagName = v
  }
  static set instanceType(v) {
    instanceType = v
  }
  
  static defaults(options) {
    request.defaults(Object.assign({resolveWithFullResponse: true}, options))
  }

  static isBlocked(response) {
    return blockedStatusCodes.includes(response.statusCode)
  }

  /**
   * プロキシサーバーをIPを元に殺す
   * @param {String} proxyIp
   */
  static killProxyServer(proxyIp) {
    // 抽出
    const instanceIdStdOut = childProcess.execSync(`aws ec2 describe-instances --query "Reservations[*].Instances[*].[InstanceId]" --filters Name=ip-address,Values=${proxyIp}`)
    const instanceId = Buffer.from(instanceIdStdOut).toString().trim()

    // 殺す
    // ほったらかし
    childProcess.exec(`aws ec2 terminate-instances --instance-ids ${instanceId}`)
  }

  static getOption(proxy) {
    const defaultOption = {
      resolveWithFullResponse: true,
      proxy: `http://${proxy}:${port}`
    }
    return defaultOption
  }

  /**
   * ランダムに一つ取得する
   */
  static getRandomProxy() {
    if (proxyList.length===0) {
      console.warn('一つもproxyがありません！！！')
      throw new Error('一つもproxyがありません！！！')
    }
    return proxyList[(new Date()).getTime() % proxyList.length]
  }

  /**
   * プロキシサーバーを列挙
   */
  static listProxyServer() {
    const stdout = childProcess.execSync(`aws ec2 describe-instances --query "Reservations[*].Instances[*].[PublicIpAddress]" --filters Name=tag:${tagName},Values='' Name=instance-state-name,Values=running`)
    const ipList = Buffer.from(stdout).toString()
    return ipList.split('\n').filter(v=>!!v)
  }

  /**
   * プロキシサーバーを立てる
   */
  static startProxyServer() {
    // ほったらかし
    childProcess.exec(`aws ec2 run-instances --image-id ${amiId} --count 1 --instance-type ${instanceType} --key-name ${sshKeyName} --security-groups ${securityGroups} --tag-specifications 'ResourceType=instance,Tags=[{Key=${tagName},Value=""}]'`)
  }
}

// 初期投入
proxyList = Asmp.listProxyServer()

module.exports = Asmp
